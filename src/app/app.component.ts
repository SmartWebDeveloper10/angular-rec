import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  pathStack = [];
  
  key: string = "children";
  
  data:Array<Object> =[
    {name:'Russia',
      children:[{name:'Vladimir Area',
      children:[
        {name:'Vladimir',
        children:[
                {title:"Uspenskii Cathedral",screen:"uspenski.JPG",description:"The cathedral was built in 12th century by Duke Andrey Bogoliubov."},
                {title:"Saints of Dmitrov Cathedral",screen:"saints.JPG",description:"Saints of Dmitrov cathedral. The cathedral was built in 12th century by Duke Andrey Bogoliubov."},
              ]
            },
        {name:'Bogoliubovo',
        children:[
              {title:"Pokrova-on-Nerl Church",screen:"face.JPG",description:"Angel's face on Pokrovaon-Nerl church. Built in 12th century."},
              {title:"Uspenskii Cathedral",screen:"uspenski.JPG",description:"The cathedral was built in 12th century by Duke Andrey Bogoliubov."}
        ]}
        ]
      },
      {name:'Moscow Area',
      children:[
        {name:'Moscow',
        children:[
                {title:"Uspenskii Cathedral",screen:"uspenski.JPG",description:"The cathedral was built in 12th century by Duke Andrey Bogoliubov."},
                {title:"Saints of Dmitrov Cathedral",screen:"saints.JPG",description:"Saints of Dmitrov cathedral. The cathedral was built in 12th century by Duke Andrey Bogoliubov."},
              ]
            },
        {name:'Kolomna',
        children:[
              {title:"Pokrova-on-Nerl Church",screen:"face.JPG",description:"Angel's face on Pokrovaon-Nerl church. Built in 12th century."},
              {title:"Uspenskii Cathedral",screen:"uspenski.JPG",description:"The cathedral was built in 12th century by Duke Andrey Bogoliubov."}
        ]}
        ]
      }
    ]
  },
  {
    name:'Spain',
    children:[{name:'Costa Bravo',
    children:[
      {name:'Barselona',
      children:[
        {title:'Dali Cathedral',screen:"",description:""}
      ]},
      {name:'Mont Serrat',
      children:[
        {title:'Madonna',screen:"",description:""}
      ]}
    ]}]
  },
  {
    name:'Italy',
    children:[{name:'Rome',
    children:[
      {name:'Forum',
      children:[
        {title:'Dali Cathedral',screen:"",description:""}
      ]},
      {name:'Colliseum',
      children:[
        {title:'Madonna',screen:"",description:""}
      ]}
    ]}]
  }
];

  
  constructor() {
   
  }

}
