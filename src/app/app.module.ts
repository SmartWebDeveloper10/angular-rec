import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import { TreeViewComponent } from './tree-view/tree-view.component';

import {IvyCarouselModule} from 'angular-responsive-carousel';
import { TreeViewStackComponent } from './tree-view-stack/tree-view-stack.component';
import { TreeLeafStackComponent } from './tree-leaf-stack/tree-leaf-stack.component';




@NgModule({
  declarations: [
    AppComponent,
    TreeViewComponent,
    TreeViewStackComponent,
    TreeLeafStackComponent,
  
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    IvyCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
