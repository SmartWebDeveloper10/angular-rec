import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StackService {

  content={
  stack : []
  }

  stack: Array<String> =[]
  constructor() { }


  getStack(){
    console.log('stack: ',this.stack)
    return this.stack;
  }

  popStack(){
    this.stack.pop();
  }

  pushStack(path){
    this.stack.push(path);
  }


}
