import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeLeafStackComponent } from './tree-leaf-stack.component';

describe('TreeLeafStackComponent', () => {
  let component: TreeLeafStackComponent;
  let fixture: ComponentFixture<TreeLeafStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeLeafStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeLeafStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
