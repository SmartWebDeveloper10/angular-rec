import { Component, Input,OnInit, ChangeDetectionStrategy } from '@angular/core';
import {StackService} from '../services/stack.service';

@Component({
  selector: 'tree-leaf-stack',
  templateUrl: './tree-leaf-stack.component.html',
  styleUrls: ['./tree-leaf-stack.component.css']
})
export class TreeLeafStackComponent implements OnInit {
  @Input('data') items: Array<Object>;
  @Input('key') key: string;
  @Input('path') path: String;
  
  fullPath: String;
  
  constructor(private stackService:StackService) { }


  ngOnInit() {
    this.fullPath = this.stackService.getStack().concat(this.path).join('/');
  }

}
