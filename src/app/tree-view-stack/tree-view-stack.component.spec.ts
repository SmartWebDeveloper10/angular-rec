import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewStackComponent } from './tree-view-stack.component';

describe('TreeViewStackComponent', () => {
  let component: TreeViewStackComponent;
  let fixture: ComponentFixture<TreeViewStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
