import { Component, OnInit, Input, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import {StackService} from '../services/stack.service';

@Component({
  selector: 'tree-view-stack',
  templateUrl: './tree-view-stack.component.html',
  styleUrls: ['./tree-view-stack.component.css']
})
export class TreeViewStackComponent implements OnInit, AfterViewInit {
  @Input('data') items: Array<Object>;
  @Input('key') key: string;
  @Input('path') path: Array<Object>;
 
  

  constructor(private stackService:StackService) { }

 
  ngAfterViewInit(): void {
   this.stackService.popStack();
  }
 

  ngOnInit() {
  this.stackService.pushStack(this.path);
  
  }
}