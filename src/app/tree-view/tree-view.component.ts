import { Component, OnInit, Input, ChangeDetectionStrategy, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import {StackService} from '../services/stack.service';

@Component({
  selector: 'tree-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent {
  @Input('data') items: Array<Object>;
  @Input('key') key: string;
  @Input('path') path: Array<Object>;
 



}
